<?php

namespace Drupal\Tests\cognito\Traits;

use Drupal\cognito\Aws\CognitoInterface;
use Drupal\cognito\Form\Email\AdminRegisterForm;
use Drupal\cognito\Form\Email\ProfileForm;
use Drupal\cognito\Form\Email\RegisterForm;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\externalauth\AuthmapInterface;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\Tests\cognito\Unit\CognitoMessagesStub;

/**
 * Trait to help test the registration form.
 */
trait RegisterFormHelper {

  /**
   * Constructs the register form.
   *
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The cognito service.
   *
   * @return \Drupal\cognito\Form\Email\RegisterForm
   *   The constructed register form.
   */
  protected function getRegisterForm(CognitoInterface $cognito) {
    return $this->injectFormDependencies(RegisterForm::class, $cognito);
  }

  /**
   * Constructs the admin register form.
   *
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The cognito service.
   *
   * @return \Drupal\cognito\Form\Email\AdminRegisterForm
   *   The constructed register form.
   */
  protected function getAdminRegisterForm(CognitoInterface $cognito) {
    return $this->injectFormDependencies(AdminRegisterForm::class, $cognito);
  }

  /**
   * Constructs the profile form.
   *
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The cognito service.
   *
   * @return \Drupal\cognito\Form\Email\ProfileForm
   *   The constructed register form.
   */
  protected function getProfileForm(CognitoInterface $cognito) {
    return $this->injectFormDependencies(ProfileForm::class, $cognito);
  }

  /**
   * Helper to inject all the dependencies needed by AccountForm.
   *
   * @param string $class
   *   The form class to instantiate.
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The cognito service.
   *
   * @return mixed
   *   The instantiated form.
   */
  protected function injectFormDependencies($class, CognitoInterface $cognito) {
    $entityManager = $this->getMock(EntityManagerInterface::class);
    $languageManager = $this->getMock(LanguageManagerInterface::class);
    $entityTypeBundle = $this->getMock(EntityTypeBundleInfoInterface::class);
    $time = $this->getMock(TimeInterface::class);
    $translation = $this->getStringTranslation();
    $externalauth = $this->getMock(ExternalAuthInterface::class);
    $authmap = $this->getMock(AuthmapInterface::class);

    return new $class($entityManager, $languageManager, $entityTypeBundle, $time, $translation, $cognito, new CognitoMessagesStub(), $externalauth, $authmap);
  }

  /**
   * Returns a stub translation manager that just returns the passed string.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject|\Drupal\Core\StringTranslation\TranslationInterface
   *   A mock translation object.
   */
  public function getStringTranslation() {
    $translation = $this->getMock('Drupal\Core\StringTranslation\TranslationInterface');
    $translation->expects($this->any())
      ->method('translate')
      ->willReturnCallback(function ($string, array $args = [], array $options = []) use ($translation) {
        // @codingStandardsIgnoreStart
        return new TranslatableMarkup($string, $args, $options, $translation);
        // @codingStandardsIgnoreEnd
      });
    $translation->expects($this->any())
      ->method('translateString')
      ->willReturnCallback(function (TranslatableMarkup $wrapper) {
        return $wrapper->getUntranslatedString();
      });
    $translation->expects($this->any())
      ->method('formatPlural')
      ->willReturnCallback(function ($count, $singular, $plural, array $args = [], array $options = []) use ($translation) {
        $wrapper = new PluralTranslatableMarkup($count, $singular, $plural, $args, $options, $translation);
        return $wrapper;
      });
    return $translation;
  }

}
