<?php

namespace Drupal\Tests\cognito\Unit\Email;

use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Aws\CommandInterface;
use Drupal\cognito\Aws\CognitoInterface;
use Drupal\cognito\Aws\CognitoResult;
use Drupal\Core\Form\FormState;
use Drupal\Tests\cognito\Traits\RegisterFormHelper;
use Drupal\Tests\PhpunitCompatibilityTrait;
use PHPUnit\Framework\TestCase;

/**
 * Test the registration form.
 *
 * @group cognito
 */
class RegisterFormTest extends TestCase {

  use RegisterFormHelper;
  use PhpunitCompatibilityTrait;

  /**
   * Test a failed validation.
   */
  public function testValidateRegistrationError() {
    $cognito = $this->getMock(CognitoInterface::class);
    $cognito
      ->method('signUp')
      ->willReturn(new CognitoResult([], new \Exception('Registration failed')));

    $formState = new FormState();
    $form = [];
    $formObj = $this->getRegisterForm($cognito);

    $formObj->validateRegistration($form, $formState);

    $errors = $formState->getErrors();
    $this->assertEquals('Registration failed', array_pop($errors));
  }

  /**
   * When we fail with a UsernameExistsException we attempt to resend.
   */
  public function testValidateRegistrationAttemptResendThatThrowsError() {
    $email = 'test@example.com';
    $command = $this->getMock(CommandInterface::class);
    $exception = new CognitoIdentityProviderException('Exception message', $command, [
      'message' => 'Unable to reset password at this time.',
      'code' => 'UsernameExistsException',
    ]);

    $cognito = $this->getMock(CognitoInterface::class);
    $cognito
      ->method('signUp')
      ->willReturn(new CognitoResult([], $exception));
    $cognito
      ->expects($this->once())
      ->method('resendConfirmationCode')
      ->with($email)
      ->willReturn(new CognitoResult([], new \Exception('Failed to resend confirmation')));

    $formState = new FormState();
    $formState->clearErrors();
    $formState->setValue('mail', $email);
    $form = [];
    $formObj = $this->getRegisterForm($cognito);

    $formObj->validateRegistration($form, $formState);

    $errors = $formState->getErrors();
    $this->assertStringStartsWith('You already have an account.', (string) array_pop($errors));
  }

  /**
   * Test a successful validation of the confirmation form.
   */
  public function testValidateConfirmation() {
    $cognito = $this->getMock(CognitoInterface::class);
    $cognito
      ->method('confirmSignup')
      ->willReturn(new CognitoResult([]));

    $formState = new FormState();
    $form = [];
    $formObj = $this->getRegisterForm($cognito);
    $property = new \ReflectionProperty($formObj, 'multistepFormValues');
    $property->setAccessible(TRUE);
    $property->setValue($formObj, ['mail' => 'test@example.com']);

    $formObj->validateConfirmation($form, $formState);

    $this->assertCount(0, $formState->getErrors());
  }

  /**
   * Test a failed validation of the confirmation form.
   */
  public function testValidateConfirmationFailed() {
    $cognito = $this->getMock(CognitoInterface::class);
    $cognito
      ->method('confirmSignup')
      ->willReturn(new CognitoResult([], new \Exception('Confirmation failed')));

    $formState = new FormState();
    $form = [];
    $formObj = $this->getRegisterForm($cognito);
    $property = new \ReflectionProperty($formObj, 'multistepFormValues');
    $property->setAccessible(TRUE);
    $property->setValue($formObj, ['mail' => 'test@example.com']);

    $formObj->validateConfirmation($form, $formState);

    $errors = $formState->getErrors();
    $this->assertCount(1, $errors);
    $this->assertEquals('Confirmation failed', array_pop($errors));
  }

}
